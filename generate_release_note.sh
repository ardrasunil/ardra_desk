#!/bin/bash

# Install jq
apt-get update -qy
apt-get install -y jq

# Variables for API call
GITLAB_API_URL="https://gitlab.com/api/v4"
ACCESS_TOKEN="glpat-5QQHBNppoxXknAGKkBfg"
PROJECT_ID="$CI_PROJECT_ID"
SOURCE_BRANCH="$CI_COMMIT_REF_NAME"
TARGET_BRANCH="release"

# Print the variables to verify
echo "ACCESS TOKEN : $ACCESS_TOKEN"
echo "GITLAB API URL : $GITLAB_API_URL"
echo "PROJECT ID : $PROJECT_ID"
echo "SOURCE BRANCH : $SOURCE_BRANCH"

# API call to get the MR ID
MR_ID=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$PROJECT_ID/merge_requests?source_branch=$SOURCE_BRANCH" | jq -r '.[0].iid')

# API call to get the commits of MR
MR_COMMIT=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/merge_requests/$MR_ID/commits" | jq -r '.[] | "\(.title) by \(.committer_name) on \(.committed_date)"')

MR_URL=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/merge_requests/$MR_IID" | jq -r '.web_url')

release_note="RELEASE NOTE
MR_ID : $MR_IID
Merge Request Url : $MR_URL

COMMITS 
$MR_COMMIT"

echo "$release_note"


